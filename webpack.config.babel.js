import path from 'path'
import nodeExternals from 'webpack-node-externals'
import nodemonPlugin from 'nodemon-webpack-plugin'

/**
 * A rule object for JavaScript(JSX) files to be loaded via an array of loaders.
 */
const babelRule = {
	test: [/\.jsx{2,}/],
	exclude: [/node_modules/],
	use: ['babel-loader']
}

/**
 * @description The default config function being used by Webpack.
 * @returns An array of configs. Client and Server. The client config
 * contains dev server configs.
 */
export default (env, argv = { mode: 'development' }) => [{
	target: 'web',
	mode: argv.mode,
	name: 'App',
	entry: {
		client: [
			path.resolve('source', 'client', 'index.jsx')
		]
	},
	output: {
		filename: '[name].bundle.js',
		chunkFilename: '[name].bundle.js',
		path: path.resolve('bundle', 'public')
	},
	module: {
		rules: [
			babelRule
		]
	},
	/**
	 * @field [publicPath]. 
	 * @field [contentBase].
	 */
	devServer: {
		publicPath: '/public/',
		contentBase: path.resolve('bundle', 'public'),
		port: 3001,
		hot: argv.mode === 'development',
		open: true,
		compress: true
	},
	optimization: {
		/**
		 * @field [chunks]. 
		 */
		splitChunks: {
			chunks: 'all'
		}
	},
}]



